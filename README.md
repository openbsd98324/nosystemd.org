## What is systemd?

systemd claims to be a good and modern replacement for SysVinit ‐ a so called init daemon. Usually the init daemon is the first process spawned by the kernel and thus has the PID #1 and is responsible for spawning other daemons which are necessary for the OS to operate, e.g. networking, cron, syslog etc.

List of init possible daemons:

systemd
SysVinit (home)
OpenRC (home)
runit (home)
s6 (home)
Shepherd (home)
finit (home)
Hummingbird (repo)
uselessd (repo; inactive)
Upstart (home; inactive)
InitNG (repo; inactive)
cinit (home; inactive)
minit (home; inactive)
Epoch (home; inactive)

The systemd project is a Red Hat initiative. The main systemd developers are Red Hat employees. It seems that to many in the Linux world, anything that comes out of the “corporate” Linux camps—Red Hat, Oracle, Intel, Canonical, for example—must automatically be distrusted.


![](media/nosystemd.png)


## Unix/BSD  


FreeBSD (Excellent, ideal for Server)

NetBSD  (Excellent Desktop)

OpenBSD (Highest Security, routers,... )

NomadBSD (Excellent as a Desktop)

(...)


## Linux distributions 

For Memstick with Linux


- DEVUAN :

 https://mirror.leaseweb.com/devuan/devuan_ascii/installer-iso/devuan_ascii_2.1_amd64_dvd-1.iso

- PCLINUXOS : 

http://mirrors.standaloneinstaller.com/pclinuxos/pclinuxos/live-cd/64bit/pclinuxos64-kde-2022.01.10.iso

Firefox: 95.0.2 (64 bits) 
linux kernel 5.15.13


- Crux 


(...)

## Linux From Scratch

- LinuxFromScratch 

Gentoo,... 

https://www.linuxfromscratch.org/


- OpenBRT :
 
https://gitlab.com/openbsd98324/openbrt



## Note

It has systemd!! 


 Debian  (newest releases)

 Ubuntu (the full systemd system) 
 with firefox 91.0 build ubuntu 20.04
 linux kernel 
  5.11.0-27 






